'use strict';

var should = require('should');
var app = require('../../../app');
var request = require('supertest');

describe('GET /api 404 Testing', function() {

  it('should exist Server', function (done) {
    should.exist(app);
    done();
  });

  it('/api with 404', function(done) {
    request(app)
      .get('/api')
      .expect(404, done);
  });

});