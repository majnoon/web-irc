'use strict';

module.exports = function(grunt) {
  // all grunt task load
  require('load-grunt-tasks')(grunt);

  require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // ExpressJS 테스팅 & 배포 환경 설정
    express: {
      options: {
        port: process.env.PORT || 9000
      },
      dev: {
        options: {
          script: 'app.js',
          debug: true
        }
      },
      prod: {
        options: {
          script: 'dist/app.js',
          node_env: 'production'
        }
      }
    },
    // 서버가 실행되면 웹페이지 자동 오픈
    open: {
      server: {
        url: 'http://localhost:<%= express.options.port %>'
      }
    },
    // 파일 변경 감시
    watch: {
      gruntfile: {
        files: ['Gruntfile.js']
      },
      js: {
        files: ['public/js/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: true
        }
      },
      mochaTest: {
        files: ['test/server/**/*.js'],
        tasks: ['env:test', 'mochaTest']
      },
      livereload: {
        files: [
          'public/js/{,*//*}*.html',
          '{.tmp, public}/css/{,*//*}*.css',
          '{.tmp, public}/js/{,*//*}*.js',
          'public/images/{,*//*}*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: {
          livereload: true
        }
      },
      express: {
        files: [
          'app.js',
          'lib/**/*.{js,json}'
        ],
        tasks: ['newer:jshint:server', 'express:dev', 'wait'],
        options: {
          livereload: true,
          nospawn: true //Without this option specified express won't be reloaded
        }
      }
    },
    // 자바스크립트 검사
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      server: {
        options: {
          jshintrc: 'lib/.jshintrc'
        },
        src: ['app.js', 'lib/{,*/}*.js']
      },
      all: ['public/js/{,*/}*.js']
    },
    // 복사 작업들
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'public',
          dest: 'dist/public',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'lib/**/*',
            'img/{,*/}*.{webp}',
            'fonts/**/*',
            '*.html',
            'js/**/*',
            'css/*.css'
          ]
        }, {
          expand: true,
          cwd: '.tmp/img',
          dest: 'dist/public/img',
          src: ['generated/*']
        }, {
          expand: true,
          dest: 'dist',
          src: [
            'package.json',
            'app.js',
            'lib/**/*'
          ]
        }]
      },
      styles: {
        expand: true,
        cwd: 'public/css',
        dest: '.tmp/css/',
        src: '{,*/}*.css'
      }
    },
    // Use nodemon to run server in debug mode with an initial breakpoint
    nodemon: {
      debug: {
        script: 'app.js',
        options: {
          nodeArgs: ['--debug-brk'],
          env: {
            PORT: process.env.PORT || 9000
          },
          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            // opens browser on initial server start
            nodemon.on('config:update', function () {
              setTimeout(function () {
                require('open')('http://localhost:8080/debug?port=5858');
              }, 500);
            });
          }
        }
      }
    },
    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      debug: {
        tasks: [
          'nodemon'
        ],
        options: {
          logConcurrentOutput: true
        }
      },
      dist: [
        'copy:styles'
      ]
    },
    // 컴파일 된 캐시들을 모조리 지운다.
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            'dist/*',
            '!dist/.git*',
            '!dist/Procfile'
          ]
        }]
      },
      server: '.tmp'
    },
    // bower 주입
    bowerInstall: {
      target: {
        src: 'public/index.html'
      }
    },
    // 작성 스크립트 주입
    fileblocks: {
      options: {
        removeFiles: true,
        cwd: 'public/'
      },
      app: {
        src: 'public/index.html',
        blocks: {
          app: { 
            src: ['js/app.js', 'js/**/*.js']
          }
        }
      }
    },
    // 테스트 환경설정
    mochaTest: {
      options: {
        reporter: 'spec'
      },
      src: ['test/server/**/*.js']
    },
    env: {
      test: {
        NODE_ENV: 'test'
      }
    }
  });

  grunt.registerTask('express-keepalive', 'Keep grunt running', function() {
    this.async();
  });

  grunt.registerTask('wait', function () {
    grunt.log.ok('서버 다시 로드하는 중...');

    var done = this.async();

    setTimeout(function () {
      grunt.log.writeln('다시 로드 완료!');
      done();
    }, 500);
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run([
        'build', 
        'express:prod', 
        'open', 
        'express-keepalive'
      ]);
    }

    if (target === 'debug') {
      return grunt.task.run([
        'concurrent:server',
        'concurrent:debug'
      ]);
    }

    grunt.task.run([
      'clean:server',
      'bowerInstall',
      'fileblocks:app',
      'concurrent:server',
      'express:dev',
      'open',
      'newer:jshint',
      'watch'
    ]);
  });

  grunt.registerTask('test', function(target) {
    if (target === 'server') {
      return grunt.task.run([
        'env:test',
        'mochaTest'
      ]);
    }
    
    else grunt.task.run([
      'newer:jshint',
      'test:server'
    ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'bowerInstall',
    'copy:dist'
  ]);

  // Default task(s).
  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);

};