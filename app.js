'use strict';

var express = require('express');
var http    = require('http');
var socket  = require('socket.io');

// 어플리케이션 모드 설정: default development
// Gruntfile.js 에서 모드를 설정.
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var config = require('./lib/config/config');
var app    = express();
var server = http.createServer(app);
var io     = socket.listen(server);
// Express 설정
require('./lib/config/express')(app);
// routes 설정
require('./lib/routes')(app);
// Web Socket 설정
require('./lib/sockets')(io);

server.listen(config.port, config.ip, function () {
    console.log('Express server listening on %s:%d, in %s mode', config.ip, config.port, app.get('env'));
});

exports = module.exports = app;