'use strict';

angular.module('common')
.factory('AuthService', ['$http', function($http){
  return {
    login: function(credentials) {
      return $http
      .post('/auth/signin', credentials, {cache: false})
      .then(function(data) {
        console.log(data);
      });
    }
  };
}]);