'use strict';

angular.module('mnemosyne.Signup')
.controller('signupCtrl', ['$scope', '$http', function($scope, $http){
  // 가입데이터 오브젝트
  $scope.signupdata = {
    email: '',
    password: '',
    confirmation: '',
    nickname: ''
  };

  // 가입하기
  $scope.signup = function() {
    $http.post('/signup', angular.toJson($scope.signupdata, true), {cache: false})
    .success(function(data, status) {
      if(status == 200) {
        alert('Success to signup');
      }
    });
    console.log('mnemosyne.Signup.signupCtrl : signup()');
  };
}])