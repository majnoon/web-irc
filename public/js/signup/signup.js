'use strict';

/**
 * Signup Module
 */
angular.module('mnemosyne.Signup')

.config(['$stateProvider', function($stateProvider) {
  $stateProvider
    .state('signup', {
      url: '/signup',
      controller: 'signupCtrl',
      templateUrl: 'js/signup/views/signup.html'
    });
}]);