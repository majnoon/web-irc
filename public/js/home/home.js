'use strict';

/**
 * Home Module
 */
angular.module('mnemosyne.Home')

.config(['$stateProvider', function($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      controller: 'homeCtrl',
      templateUrl: 'js/home/views/home.html'
    });
}]);