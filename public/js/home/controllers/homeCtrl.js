'use strict';

angular.module('mnemosyne.Home')
.controller('homeCtrl', ['$scope', '$http', 'AuthService', function($scope, $http, AuthService) {
    $scope.credentials = {
        email: '',
        password: ''
    };

    $scope.login = function(credentials) {
        console.log('mnemosyne.Home.homeCtrl : login()');
        AuthService.login(credentials).then(function() {
            console.log('성공');
        },
        function() {
            console.log('실패');
        });
        /*
        $http.post('/auth/signin', angular.toJson(credentials, true), {cache: false})
            .success(function(data, status) {
                if(status == 200) {
                    alert('good');
                }
            });
        */
    };
}]);