menmosyne Web IRC
====================

Require
---------------------

    $ npm install -g bower
    $ npm install -g grunt-cli

Installation
---------------------

    $ npm install
    $ bower install

Start!
---------------------

*Development*

    $ grunt serve

*Debug*

    $ grunt serve:debug

*Production*

    $ grunt serve:dist

*Testing*

    $ grunt test

Generate Self-sign Certification Guide
---------------------

  PEM Passphrase : 1234
  [Guide](http://bsdbox.co/2013/12/20/https-generate-ssl-certificate/)

We're Have some List!
---------------------

  * ~~Auto Testing Settup.~~
  * ~~Add Socket.io~~
  * Mocking Test.
  * Perfectly Production Mode.
  * SSL Settup in Production Mode.
