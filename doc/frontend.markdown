Front-end Struture



```list
    ├── public
    │   ├── index.html
    │   │
    │   ├── js
    │   │   ├── app.js
    │   │   │
    │   │   ├── home
    │   │   │   ├── home.js
    │   │   │   ├── controllers
    │   │   │   │   └── homeCtrl.js
    │   │   │   └── views
    │   │   │       └── home.html
    │   │   │
    │   │   └── signup
    │   │       ├── signup.js
    │   │       ├── controllers
    │   │       │   └── signupCtrl.js
    │   │       └── views
    │   │           └── signup.html
    │   ├── css
    │   └── lib
```