-- Database: neris

-- DROP DATABASE neris;

CREATE DATABASE neris
  WITH OWNER = neris
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'ko_KR.UTF-8'
       LC_CTYPE = 'ko_KR.UTF-8'
       CONNECTION LIMIT = -1;
GRANT ALL ON DATABASE neris TO public;
GRANT ALL ON DATABASE neris TO neris;

ALTER DEFAULT PRIVILEGES
    GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON TABLES
    TO public;

ALTER DEFAULT PRIVILEGES
    GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON TABLES
    TO postgres;


-- Sequence: channels_cnt

-- DROP SEQUENCE channels_cnt;

CREATE SEQUENCE channels_cnt
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE channels_cnt
  OWNER TO neris;

-- Sequence: line_cnt

-- DROP SEQUENCE line_cnt;

CREATE SEQUENCE line_cnt
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE line_cnt
  OWNER TO neris;
GRANT ALL ON TABLE line_cnt TO postgres;
GRANT ALL ON TABLE line_cnt TO public;

-- Sequence: users_cnt

-- DROP SEQUENCE users_cnt;

CREATE SEQUENCE users_cnt
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE users_cnt
  OWNER TO neris;



-- Table: channels

-- DROP TABLE channels;

CREATE TABLE channels
(
  "number" integer DEFAULT nextval('channels_cnt'::regclass),
  channelname text NOT NULL,
  CONSTRAINT channels_pkey PRIMARY KEY (channelname),
  CONSTRAINT name_uc UNIQUE (channelname)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE channels
  OWNER TO neris;
GRANT ALL ON TABLE channels TO public;
GRANT ALL ON TABLE channels TO neris;

-- Table: irc_log

-- DROP TABLE irc_log;

CREATE TABLE irc_log
(
  id integer NOT NULL DEFAULT nextval('line_cnt'::regclass),
  "time" timestamp without time zone,
  type integer,
  channel text,
  nick text,
  what text,
  CONSTRAINT irc_log_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE irc_log
  OWNER TO neris;
GRANT ALL ON TABLE irc_log TO neris;
GRANT ALL ON TABLE irc_log TO public;

-- Index: log_idx_time

-- DROP INDEX log_idx_time;

CREATE INDEX log_idx_time
  ON irc_log
  USING btree
  ("time", channel COLLATE pg_catalog."default");

-- Table: users

-- DROP TABLE users;

CREATE TABLE users
(
  user_id bigint NOT NULL DEFAULT nextval('users_cnt'::regclass),
  email text NOT NULL,
  password text NOT NULL,
  nickname text NOT NULL,
  usertype text NOT NULL,
  status integer NOT NULL,
  signup_date timestamp without time zone,
  CONSTRAINT users_key PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO neris;
GRANT ALL ON TABLE users TO public;
GRANT ALL ON TABLE users TO neris;

-- Index: users_username_password_idx

-- DROP INDEX users_username_password_idx;

CREATE INDEX users_email_password_idx
  ON users
  USING btree
  (email COLLATE pg_catalog."default", password COLLATE pg_catalog."default");

-- Table: privilege

-- DROP TABLE privilege;

CREATE TABLE privilege
(
  user_id bigint NOT NULL,
  channel text NOT NULL,
  CONSTRAINT privilege_pkey PRIMARY KEY (user_id, channel),
  CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE privilege
  OWNER TO neris;
GRANT ALL ON TABLE privilege TO public;
GRANT ALL ON TABLE privilege TO neris;