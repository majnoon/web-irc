'use strict';

var path          = require('path');
var express       = require('express');
var favicon       = require('static-favicon');
var logger        = require('morgan');
var compression   = require('compression');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var session       = require('express-session');
var redisStore    = require('connect-redis')(session);
var redis         = require('redis');
var config        = require('./config');

/**
 * Express 설정
 */
module.exports = function(app) {
    var env = app.get('env');

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());
    app.use(cookieParser());
    app.use(express.static(path.join(config.root, 'public')));

    if ('development' === env) {
        app.use(require('connect-livereload')());

        // Disable caching of scripts for easier testing
        app.use(function noCache(req, res, next) {
            if (req.url.indexOf('/js/') === 0) {
               res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
               res.header('Pragma', 'no-cache');
               res.header('Expires', 0);
            }
            next();
        });

        // 500 에러가 났을 경우 로깅
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });

        app.use(express.static(path.join(config.root, '.tmp')));
        app.use(logger('dev'));

        //development 모드에서 일반 세션 스토어 사용
        app.use(session({ secret: 'mnemosyne loves neris', key: 'mid', cookie: { secure: true }}));
    }

    if ('production' === env) {
        app.use(compression());
        app.use(favicon(path.join(config.root, 'public', 'favicon.ico')));
        // 500 에러가 났을 경우 로깅
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: {}
            });
        });

        // production 모드에서 Redis Session Store 사용.
        app.use(session({
            store: new redisStore({
                url: "redis://localhost:6379/neris"
            }),
            secret: 'mnemosyne loves neris'
        }));
    }
};