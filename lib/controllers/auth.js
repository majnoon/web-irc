'use strict';

var users = require('../models/users.js');
var bcrypt = require('bcrypt');

exports.signin = function(req, res) {
    var userdata = req.body;
    console.log('userdata: %j', userdata);

    if(typeof(userdata) === undefined)
        res.send(400, {'status': false});

    users.get_info_by_email(userdata, function (err, userinfo) {
        if (err) {
            console.log('error reason: ' + err.message);
            res.send(403, {
                status: false
            });

            return;
        }

        if (userinfo === null ||
            !bcrypt.compareSync(userdata.password, userinfo.password)) {
            console.log('Incorrect login data.');
            res.send(403, {
                status: false,
                reason: 'Please make sure you enter your email address and password correctly.'
            });

            return;
        }

        req.authenticated = true;
        req.session.user = userinfo;
        req.session.user.email = userdata.email.toLowerCase();

        res.send({
            status: true,
            data: {
                nickname: userinfo.nickname
            }
        });
    });
};

exports.signout = function(req, res) {
    if (req.authenticated) {
        req.session.destroy(function () {

        });
        res.send({
            status: true
        });
    }
    else {
        res.send({
            status: false,
            reason: 'You didn\'t authenticate.'
        });
    }
};