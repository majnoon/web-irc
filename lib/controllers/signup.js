'use strict';

var users = require('../models/users.js');
var bcrypt = require('bcrypt');
var salt = bcrypt.genSaltSync(10);

exports.signup = function(req, res) {
    var userdata = req.body;
    console.log(userdata);

    if (typeof(userdata) === undefined ||
        userdata.password !== userdata.confirmation) {
        res.send(400, {
            'status': false,
            'reason': 'Invalid data'
        });
        return;
    }

    userdata.email = userdata.email.toLowerCase();
    userdata.password = bcrypt.hashSync(userdata.password, salt);

    users.is_exist_user_by_email( { email: userdata.email }, function (err, is_exist) {
        if (err) {
            res.json(403, {
                status: false,
                reason: err.message
            });
            return;
        }
        if (is_exist) {
            res.json(403, {
                'status': false,
                'reason': 'Already exist email.'
            });

            return;
        }

        users.insert_user(userdata, function (err, is_success) {
            if (err) {
                res.json(403, {
                    status: false,
                    reason: err.message
                });
                return;
            }

            if (is_success === false) {
                return res.send({
                    status: false
                });
            }

            // Success
            res.json(userdata);
        });
    });
};