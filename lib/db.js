/**
 * Created by envi on 2014-05-04.
 */
var pg = require('pg');
var sys = require('sys');
var conf = new require('./config/config');

var dbconn;
var connString =  conf.db_url;

exports.connect = function() {
    if (!dbconn) {
        dbconn = new pg.Client(connString);
        dbconn.connect();
    }
    return dbconn;
};

exports.disconnect = function() {
    if (dbconn) {
        dbconn.disconnect();
        dbconn = null;
    }
};

exports.after = function(callback) {
    return function(err, queryResult) {
        if(err) {
            console.log('DB ERROR : ', err);
        }
        callback(queryResult);
    };
};
